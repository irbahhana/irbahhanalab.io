# Persoal Website

Muhammad Naufal Irbahhana's personal static website created with React. This website mostly consist of professional
experience and projects that I have built and incorporate in. 

## Local Setup

```
$ npm install
$ npm run start
```

## License

[MIT]()
