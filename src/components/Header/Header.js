import React, {useRef} from "react";
import TextLoop from "react-text-loop";
import {IntroWrapper} from "./style";
import {IconButton} from "@material-ui/core";
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';
import EmailIcon from '@material-ui/icons/Email';
import {Icon} from '@iconify/react';
import spotifyIcon from '@iconify-icons/mdi/spotify';

import {Link} from "react-router-dom";

const Header = () => {

    const position = useRef(0)

    const TEXT = [
        "Hello,",
        "Hola,",
        "Guten Tag,",
        "السَّلاَمُ عَلَيْكُمْ",
        "안녕하세요,",
        "こんにちは",
    ]

    return (
        <IntroWrapper ref={position} yposition={0.5 - window.pageYOffset}>
            <div className="content">
                <h1>
                    <TextLoop interval={2000} children={TEXT}>
                    </TextLoop>{"\n"}

                </h1>
                <h1>
                    I'm Naufal
                </h1>
                <div style={{maxWidth:"500px"}}>
                    <pre className="desc">
                        Final year computer science students at University of Queensland. I love to ideate, build, and fix problems. Yes, I love to <a style={{color:"#fff9ec"}} href={"https://open.spotify.com/album/2oMbQ7W1QddUdasTYrJdzE?autoplay=true"}>build problems</a>.
                    </pre>
                </div>

                <div className="socials">
                    <IconButton onClick={(e) => window.open("https://www.linkedin.com/in/irbahhana/", "_blank")}
                                className="icons" color="secondary" aria-label="LinkedIn">
                        <LinkedInIcon style={{color: "#FFB8D1"}} fontSize={"large"}/>
                    </IconButton>
                    <IconButton onClick={(e) => window.open("https://gitlab.com/irbahhana", "_blank")}
                                className="icons"
                                color="secondary" aria-label="Github">
                        <GitHubIcon style={{color: "#FFB8D1"}} fontSize={"large"}/>
                    </IconButton>
                    <IconButton
                        onClick={(e) => window.open("https://open.spotify.com/user/irbahhana?si=0644ad1f3778443d", "_blank")}
                        className="icons" color="secondary" aria-label="Github">
                        <Icon icon={spotifyIcon} style={{width: "2.5rem", height: "2.5rem", color: "#FFB8D1"}}/>
                    </IconButton>
                    <IconButton style={{color: "#FFB8D1"}} component={Link} to='#'
                                onClick={(e) => {
                                    window.location = "mailto:irbahananaufal@gmail.com";
                                    e.preventDefault();
                                }} className="icons" color="secondary" aria-label="Github">
                        <EmailIcon style={{color: "#FFB8D1"}} fontSize={"large"}/>
                    </IconButton>
                </div>
            </div>
        </IntroWrapper>
    );
};


export default Header
