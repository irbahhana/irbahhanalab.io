import React from "react";
import style from 'styled-components'

export const IntroWrapper = style.div`
    width: 100vw;
    height 100vh;
    max-width:100%;
    display: flex;
    justify-content: center;
    white-space:pre;
    font-size: 150%;

    
    .content{
        display:flex;
        justify-content: center;
        flex-direction: column;
        padding-right:1rem;
        padding-left:1rem;
    }
    
    h1 {
        color: #FFF9EC;
        margin-block-start:0;
        margin-block-end:0;
        font-size:4rem;
        white-space: pre-wrap;
        word-wrap: break-word;
    }
    pre.desc {
        color: #FFF9EC;
        white-space: pre-wrap;
        word-wrap: break-word;
        text-align: justify;
    }
    
    .socials {
        display: flex;
    }
    
    .loopTitle{
        display:flex;
        justify-content:flex-start;
    }
`
