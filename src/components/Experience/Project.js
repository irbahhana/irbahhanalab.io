import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card, CardActions, CardContent, CardMedia, IconButton} from "@material-ui/core";
import {Icon} from '@iconify/react';

import pythonIcon from '@iconify-icons/cib/python';
import Button from "@material-ui/core/Button";
import djangoIcon from '@iconify-icons/cib/django';
import postgresqlIcon from '@iconify-icons/cib/postgresql';
import reactIcon from '@iconify-icons/cib/react';
import javascriptIcon from '@iconify-icons/cib/javascript';
import phpIcon from '@iconify-icons/cib/php';
import html5Shield from '@iconify-icons/cib/html5-shield';
import csswizardryIcon from '@iconify-icons/cib/csswizardry';


import Fableous from '../../static/images/fableous.png'
import Inacademi from '../../static/images/inacademi.png'
import Scootit from '../../static/images/scootit.png'
import Moodle from '../../static/images/moodle.png'
import Happyfresh from '../../static/images/happyfresh.png'
import Webby from '../../static/images/webby.png'

const useStyles = makeStyles({
    title: {
        color: "#FFF9EC",
        marginBlockStart: "0",
        marginBlockEnd: "0",
        fontSize: "2rem",
    },
    card: {
        minWidth: "100px",
        minHeight:"530px",
        maxWidth:"390px",
        maxHeight: "550px",
        backgroundColor: "#333742",
        display: "flex",
        flexDirection: "column"
    },
    onePad: {
        paddingTop: "1rem"
    },
    description: {
        color: "#FFF9EC",
        marginBlockStart: "1rem",
        marginBlockEnd: "0",
        whiteSpace: "pre-wrap",
        wordWrap: "break-word",
        textAlign: "justify"
    },
    pill: {
        textTransform: "none",
        backgroundColor: "transparent",
        color: "#fff9ec",
        boxShadow: "0px 0px 0px 0px",
        "&:hover": {
            backgroundColor: "#2e3138"
        }
    },
    stack: {
        display: "flex",
        justifyContent: "flex-start",
    },
    link: {
        textTransform: "none",
    },
    wrapper:{
        paddingRight:"1rem",
        paddingBottom:"1rem"
    },
    cardAction:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: '16px',
    }
})
const Project = (data) => {
    const classes = useStyles()
    const proj = data.data

    const picker = (stck) => {
        switch (stck) {
            case "Django":
                return <IconButton>
                    <Icon icon={djangoIcon} style={{color: "#2ba977"}}/>
                </IconButton>


            case "Python":
                return <IconButton>
                    <Icon icon={pythonIcon} style={{color: "#ffd542"}}/>
                </IconButton>
            case "Postgresql":
                return <IconButton>
                    <Icon icon={postgresqlIcon} style={{color: "#2398f7"}}/>
                </IconButton>
            case "React":
                return <IconButton>
                    <Icon icon={reactIcon} style={{color: "#00d8ff"}}/>
                </IconButton>
            case "Javascript":
                return <IconButton>
                    <Icon icon={javascriptIcon} style={{color: "#f7df1e"}}/>
                </IconButton>
            case "PHP":
                return <IconButton>
                    <Icon icon={phpIcon} style={{color: "#6280b6"}}/>
                </IconButton>
            case "HTML":
                return <IconButton>
                    <Icon icon={html5Shield} style={{color: "#f1662a"}}/>
                </IconButton>
            case "CSS":
                return <IconButton>
                    <Icon icon={csswizardryIcon} style={{color: "#f1662a"}}/>
                </IconButton>
            default:
                return <Button disableRipple={true} className={classes.pill} variant="contained" color="secondary"
                               size={"small"}>
                    {stck}
                </Button>
        }
    }

    const imagePicker = (img) => {
        switch (img) {
            case "Fableous":
                return Fableous
            case "Inacademi":
                return Inacademi
            case "Scoot it!":
                return Scootit
            case "Moodle Attendance Plugin":
                return Moodle
            case "HappyFresh Fleet Tracker":
                return Happyfresh
            case "Webby":
                return Webby
            default:
                return ""
        }
    }

    return (
        <div className={classes.wrapper}>
            <Card className={classes.card}>
                {
                    proj.img ?
                        <CardMedia
                            component="img"
                            image={imagePicker(proj.name)}
                            title=""
                            style={{maxWidth: "100%",height:"200px", alignItems: "center", display: "flex" }}
                        />
                        :
                        <CardMedia
                            component="img"
                            image=""
                            title=""
                            style={{maxWidth: "100%"}}
                        />
                }

                <CardContent>
                    <h1 className={classes.title}>
                        {proj.name}
                    </h1>
                    <div>
                        <p className={classes.description}>
                            {proj.description}
                        </p>
                    </div>
                    <div style={{marginTop: "1rem"}}/>
                    <div className={classes.stack}>
                        {
                            proj.stack.map((s) => picker(s))
                        }
                    </div>
                </CardContent>
                <div style={{flex:1}}/>
                <CardActions className={classes.cardAction}>
                    <div style={{flexGrow: 1}}/>
                    <Button href={proj.relatedLink} className={classes.link} variant={"contained"} size="medium">Learn
                        More</Button>
                </CardActions>
            </Card>
        </div>
    );
};

export default Project;
