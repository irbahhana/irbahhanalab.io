import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Box, Button, Card, CardActions, CardContent, Container} from "@material-ui/core";

const useStyles = makeStyles({

    root: {
        display: "flex",
        paddingTop: "5rem",
        flexDirection: "column",
        justifyContent: "center",
    },
    title: {
        color: "#FFF9EC",
        marginBlockStart: "0",
        marginBlockEnd: "0",
        fontSize: "3rem",
    },
    card: {
        maxWidth: "50rem",
        backgroundColor: "#333742"
    },
    onePad: {
        paddingTop: "1rem"
    },
})

const Work = (data) => {
    const classes = useStyles()
    const retrivedData = data.data

    const bull = (
        <Box
            component="span"
            sx={{display: 'inline-block', mx: '2px', transform: 'scale(0.8)'}}
        >
            •
        </Box>
    );

    const positionPicker = (pos) => {
        switch (pos) {
            case "Intern":
                return (
                    <Button color="secondary" size="small">
                        Intern
                    </Button>
                )
            case "Part Time":
                return (
                    <Button color="primary" size="small">
                        Part Time
                    </Button>
                )
            case "Volunteer":
                return (
                    <Button color="success" size="small">
                        Volunteer
                    </Button>
                )
        }
    }

    return (
        <div>
            {

                retrivedData.map((exp, index) => {
                    return (
                        <>
                            <Card className={classes.card}>
                                <Container>
                                    <CardContent>
                                        <h2 style={{
                                            color: '#FFF9EC',
                                            marginBlockEnd: 0,
                                            marginBlockStart: 0
                                        }}>{exp.title}</h2>
                                        <p style={{color: '#cacaca', marginBlockStart: 0}}>
                                            {exp.start} - {exp.end}
                                        </p>
                                        <p style={{color: '#FFF9EC',}}>
                                            {exp.company} {bull} {positionPicker(exp.position)}
                                        </p>
                                        <ul>
                                            {
                                                exp.description.map(
                                                    desc => {
                                                        return (
                                                            <li style={{color: "#FFF9EC"}}>
                                                                {desc}
                                                            </li>
                                                        )
                                                    }
                                                )
                                            }
                                        </ul>
                                    </CardContent>
                                    {
                                        exp.relatedLink.length === 0 ? null :
                                            <div style={{display:"flex", flexDirection:"row-reverse"}}>
                                                <CardActions>
                                                    <Button variant="contained" color="primary" href={exp.relatedLink[0]} size="small">Related
                                                        Link</Button>
                                                </CardActions>
                                            </div>

                                    }
                                </Container>
                            </Card>
                            <div className={classes.onePad}/>
                        </>
                    )
                })
            }
        </div>
    );
};

export default Work;
