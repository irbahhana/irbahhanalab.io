import React from "react";
import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import SchoolIcon from '@material-ui/icons/School';

const education = () => {
    return (
        <VerticalTimeline>
            <VerticalTimelineElement
                layout={"1-column-right"}
                position={"right"}
                contentStyle={{background: '#efd006', color: '#fff'}}
                contentArrowStyle={{borderRight: '7px solid  #efd006'}}
                date="2017 - Present"
                iconStyle={{background: '#efd006', color: '#333742'}}
                icon={<SchoolIcon/>}
            >

                <h3 className="vertical-timeline-element-title" style={{color:'#333742'}}>
                    University of Indonesia
                </h3>
                <p style={{color:'#333742'}}>
                    Computer Science major
                </p>
            </VerticalTimelineElement>
            <VerticalTimelineElement
                position={"right"}
                layout={"1-column-right"}
                contentStyle={{background: '#333742', color: '#fff'}}
                contentArrowStyle={{borderRight: '7px solid  #333742'}}
                date="2020"
                iconStyle={{background: '#333742', color: '#fff'}}
                icon={<SchoolIcon/>}
            >
                <h3 className="vertical-timeline-element-title">
                    Data Science Bootcamp
                </h3>
                <p>
                    Data science bootcamp by COMPFEST
                </p>
            </VerticalTimelineElement>

            <VerticalTimelineElement
                position={"right"}
                layout={"1-column-right"}
                contentStyle={{background: '#630094', color: '#fff'}}
                contentArrowStyle={{borderRight: '7px solid  #630094'}}
                date="2020 - Present"
                iconStyle={{background: '#630094', color: '#fff'}}
                icon={<SchoolIcon/>}
            >
                <h3 className="vertical-timeline-element-title">
                    University of Queensland
                </h3>
                <p>
                    Information Technology Major
                </p>
            </VerticalTimelineElement>


        </VerticalTimeline>
    )
}

export default education
