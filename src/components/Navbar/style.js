import React from "react";
import style from 'styled-components'

export const NavbarContainer = style.div`
    width: 100vw;
    max-width:100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    
    .menuBar{
    margin-right:20px;
    }
    
    .hello{
        margin-left:20px;
    }
`
