import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import useScrollTrigger from '@material-ui/core/useScrollTrigger';

import Slide from '@material-ui/core/Slide';
import {Link} from "react-router-dom";

import Button from "@material-ui/core/Button";
import {IconButton} from "@material-ui/core";
import FingerprintIcon from '@material-ui/icons/Fingerprint';

function HideOnScroll(props) {
    const {children} = props;
    const trigger = useScrollTrigger({target: window});

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    upperColor:{
        color:"#FFB8D1"
    }
}));

const Navbar = (props) => {
    const classes = useStyles()
    return (
        <HideOnScroll {...props}>
            <AppBar style={{background: '#333742', boxShadow: 'none'}}>
                <Toolbar>
                    <IconButton component={Link} to={'/'}>
                        <FingerprintIcon className={classes.upperColor}  fontSize={"large"}/>
                    </IconButton>
                    <div style={{flexGrow: 1}}/>
                    <Button disableRipple  className={classes.upperColor} component={Link} to={'/experience'}> Experience</Button>
                    <Button disableRipple  className={classes.upperColor} component={Link} to={'/projects'}> Project</Button>

                </Toolbar>
            </AppBar>
        </HideOnScroll>
    )
}

export default Navbar;
