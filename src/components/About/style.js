import React from "react";
import style from 'styled-components'

export const ExperienceWrapper = style.div`
    width: 100vw;
    height 100vh;
    max-width:100%;
    display: flex;
    justify-content: center;
    align-items: center;
    white-space:pre;
    .card {
          width: 45ch;
          height: 35ch;
          border-radius: 5px;
          background-size: cover;
          background-position: center center;
          box-shadow: 0px 10px 30px -5px rgba(0, 0, 0, 0.3);
          transition: box-shadow 0.5s;
          will-change: transform;
          border: 15px solid white;
                     overflow-wrap: break-word;
    }
  .textBox{
           display:flex
           width:45ch;
           height:10rem;
            white-space: pre-wrap;
  }
.card:hover {
  box-shadow: 0px 30px 100px -10px rgba(0, 0, 0, 0.4);
}
`

