import React from "react"
import {ExperienceWrapper} from "./style";
import {useSpring, animated} from 'react-spring'

const calc = (x, y) => [-(y - window.innerHeight / 2) / 20, (x - window.innerWidth / 2) / 20, 1.1]
const trans = (x, y, s) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`


const about = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [props, set] = useSpring(() => ({xys: [0, 0, 1], config: {mass: 5, tension: 350, friction: 40}}))

    return (
        <ExperienceWrapper>
            <animated.div
                className="card"
                onMouseMove={({clientX: x, clientY: y}) => set({xys: calc(x, y)})}
                onMouseLeave={() => set({xys: [0, 0, 1]})}
                style={{transform: props.xys.interpolate(trans)}}
            >
                <h1> About Me </h1>
                <div className='textBox'>
                    <p> A person with demonstrated history of working in the information technology and services
                        industry. Skilled in Python, Foreign Languages, Editing, Public Speaking, and Graphic
                        Design.</p>
                    <p> Strong Programming skill with a Bachelor's degree focused in Computer Science from University of Indonesia
                    and University of Queensland Majoring in Software Design</p>
                </div>
            </animated.div>
        </ExperienceWrapper>
    )
}

export default about