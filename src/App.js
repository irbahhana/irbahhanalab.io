import React from 'react';
import './App.css';
import Navbar from "./components/Navbar/Navbar";
import Experience from "./pages/experience";
import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import Landing from "./pages/landing";
import Projects from "./pages/projects";
import {createMuiTheme, ThemeProvider} from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#ffb8d1"
        },
        secondary:{
            main: "#fff9ec"
        }
    }
})

function App() {
    return (
        <ThemeProvider theme={theme}>
            <Router>
                <div className={"wrapper"}>
                    <Navbar/>
                    <Switch>
                        <Route path={"/"} exact={true}>
                            <Landing/>
                        </Route>
                        <Route path={"/experience"} exact={true}>
                            <Experience/>
                        </Route>
                        <Route path={"/projects"} exact={true}>
                            <Projects/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        </ThemeProvider>
    );
}

export default App;
