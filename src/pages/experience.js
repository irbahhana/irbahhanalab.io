import React from 'react';
import Education from "../components/Experience/Education";
import {makeStyles} from "@material-ui/core/styles";
import Container from '@material-ui/core/Container';
import Work from "../components/Experience/Work";
import WorkList from "../data/worklist.json"
import VolunteerList from "../data/voulntary.json"



const useStyles = makeStyles({

    root: {
        display: "flex",
        paddingTop: "5rem",
        flexDirection: "column",
        justifyContent: "center",
    },
    title: {
        color: "#FFF9EC",
        marginBlockStart: "0",
        marginBlockEnd: "0",
        fontSize: "3rem",
    },
    twoPadding:{
        paddingTop:"2rem"
    },
    wrapper:{
        display:"flex",
        flexDirection:"column",
        alignItems:"center"
    }
})

const Experience = () => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Container>
                <div>
                    <h1 className={classes.title}>
                        Education
                    </h1>
                    <Education/>
                </div>
                <div className={classes.twoPadding}/>
                <hr/>
                <div className={classes.twoPadding}/>
                <h1 className={classes.title}>
                    Work Experience
                </h1>
                <div className={classes.twoPadding}/>
                <div className={classes.wrapper}>
                    <Work data={WorkList}/>
                </div>
                <hr/>
                <div className={classes.twoPadding}/>
                <h1 className={classes.title}>
                    Volunteering
                </h1>
                <div className={classes.twoPadding}/>
                <div className={classes.wrapper}>
                    <Work data={VolunteerList}/>
                </div>

            </Container>
        </div>
    );
};

export default Experience;
