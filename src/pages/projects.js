import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Project from "../components/Experience/Project";
import data from "../data/projects.json";


const useStyles = makeStyles({

    root: {
        display: "flex",
        paddingTop: "5rem",
    },
    title: {
        color: "#FFF9EC",
        marginBlockStart: "0",
        marginBlockEnd: "0",
        fontSize: "3rem",
    },
    subtitle: {
        color: "#FFF9EC",
        whiteSpace: "pre-wrap",
        wordWrap: "break-word",
        textAlign: "justify",
        fontWeight: "normal"
    },
    link: {
        color: "#9ee1fb"
    },
    projectWrapper: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-between",
    }
})

const Projects = () => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Container>
                <h1 className={classes.title}>
                    Projects
                </h1>
                <h3 className={classes.subtitle}>
                    These are the projects that I've been working/have worked on, feel free to browse
                    and criticize my code by <a className={classes.link} href="mailto:irbahananaufal@gmail.com">mailing
                    me</a> very much appreciated.
                </h3>
                <div className={classes.projectWrapper}>
                    {
                        data.map((d) => {
                            return <Project data={d}/>
                        })
                    }
                </div>

            </Container>
        </div>
    );
};

export default Projects;
